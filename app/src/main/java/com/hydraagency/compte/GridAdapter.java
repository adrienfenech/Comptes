package com.hydraagency.compte;

import android.app.Activity;
import android.graphics.Color;
import android.view.Gravity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;

/**
 * Created by yolo on 10/31/14.
 */
public class GridAdapter extends BaseAdapter {
    public GridAdapter(Activity context, int width, int height) {
        first = true;
        this.context = context;
        this.width = width;
        this.height = height;
        size = width / 3 - width / 50;
    }

    @Override
    public int getCount() {
        return Information.getInstance().getPeople().size();
    }

    @Override
    public Object getItem(int position) {
        return Information.getInstance().getPeople().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        LinearLayout ll = new LinearLayout(context);


        GridView.LayoutParams params = new GridView.LayoutParams(
                size,
                size
        );
        ll.setLayoutParams(params);
        ll.setGravity(Gravity.CENTER);
        ll.setOrientation(LinearLayout.VERTICAL);
        ll.setClickable(false);

        View.OnClickListener listener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            }
        };



        LinearLayout ll2 = new LinearLayout(context);
        ll2.setLayoutParams(new GridView.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT,
                size / 5 * 3
        ));
        ll2.setGravity(Gravity.CENTER);
        ll2.setOrientation(LinearLayout.HORIZONTAL);
        ll2.setClickable(false);
        ll2.setBackgroundColor(Color.argb(0, 255, 255, 255));

        ImageButton head = new ImageButton(context);
        head.setPadding(8, 8, 8, 8);
        head.setLayoutParams(new GridView.LayoutParams(
                size / 3,
                size / 3));
        head.setBackgroundColor(Color.argb(0, 255, 255, 255));
        head.setImageResource(R.drawable.defaultface);
        head.setScaleType(ImageView.ScaleType.CENTER_CROP);
        head.setOnClickListener(listener);

        ImageButton pen = new ImageButton(context);
        pen.setPadding(8, 8, 8, 8);
        pen.setLayoutParams(new GridView.LayoutParams(
                size / 6,
                size / 6));
        pen.setBackgroundColor(Color.argb(0, 255, 255, 255));
        pen.setImageResource(R.drawable.editbtn);
        pen.setScaleType(ImageView.ScaleType.CENTER_CROP);
        pen.setOnClickListener(listener);

        ll2.addView(head);
        ll2.addView(pen);

        Button tv = new Button(context);
        tv.setLayoutParams(new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        ));
        tv.setText(Information.getInstance().getPeople().get(position).getName());
        tv.setBackgroundColor(Color.argb(0, 255, 255, 255));
        tv.setOnClickListener(listener);
        tv.setTextSize(15);

        ll.addView(ll2);
        ll.addView(tv);
        ll.setBackgroundResource(R.drawable.personbackground);
        return ll;
    }

    private Activity    context;
    boolean first;
    int width, height, size;
}
