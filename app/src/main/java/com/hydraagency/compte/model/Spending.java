package com.hydraagency.compte.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by yolo on 12/8/14.
 */
@DatabaseTable(tableName = "spending")
public class Spending {
    public Spending(double money, String comment) {
        this.money = money;
        this.comment = comment;
    }


    /** Getter/Setter
     *
     */

    public long getSpending_ID() {
        return id;
    }

    public double getMoney() {
        return money;
    }

    public String getComment() {
        return comment;
    }

    /** A/D
     *
     */
    @DatabaseField(generatedId = true)
    private long     id;
    @DatabaseField(columnName = "money", canBeNull = false)
    private double   money;
    @DatabaseField(columnName = "comment", canBeNull = false)
    private String   comment;
}
