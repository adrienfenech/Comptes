package com.hydraagency.compte.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by yolo on 12/8/14.
 */
@DatabaseTable(tableName = "myeventpersonspendingfor")
public class MyEventPersonSpendingFor {

    /** Getter/Setter
     *
     */

    public long getPerson_ID() {
        return person_ID;
    }

    public long getMyEventPersonSpendingBy_ID() {
        return myEventPersonSpendingBy_ID;
    }

    public long getMyEventPersonSpendingFor_ID() {
        return id;
    }

    /** A/D
     *
     */
    @DatabaseField(generatedId = true)
    private long id;
    @DatabaseField(columnName = "myeventpersonspendingbyid", canBeNull = false)
    private long myEventPersonSpendingBy_ID;
    @DatabaseField(columnName = "personid", canBeNull = false)
    private long person_ID;
}
