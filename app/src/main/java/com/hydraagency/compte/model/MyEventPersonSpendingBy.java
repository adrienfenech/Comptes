package com.hydraagency.compte.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by yolo on 12/8/14.
 */
@DatabaseTable(tableName = "myeventpersonspendingby")
public class MyEventPersonSpendingBy {

    /** Getter/Setter
     *
     */

    public long getMyEvent_ID() {
        return myEvent_ID;
    }

    public long getPerson_ID() {
        return person_ID;
    }

    public long getSpending_ID() {
        return spending_ID;
    }

    public long getMyEventPersonSPendingBy_ID() {
        return id;
    }

    /** A/D
     *
     */
    @DatabaseField(generatedId = true)
    private long id;
    @DatabaseField(columnName = "myeventid", canBeNull = false)
    private long myEvent_ID;
    @DatabaseField(columnName = "personid", canBeNull = false)
    private long person_ID;
    @DatabaseField(columnName = "spendingid", canBeNull = false)
    private long spending_ID;
}
