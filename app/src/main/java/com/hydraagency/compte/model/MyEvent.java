package com.hydraagency.compte.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by yolo on 11/27/14.
 */
@DatabaseTable(tableName = "myevent")
public class MyEvent {
    public MyEvent(String name, ArrayList<Person> people, float money, String currency, int pictureId) {
        this.name = name;
        this.currency = currency;
        this.picture_ID = pictureId;
        this.date = new Date().toString();
    }

    public String getDate() {
        return date;
    }

    public long getMyEvent_ID() {
        return id;
    }

    public int getPicture_ID() {
        return picture_ID;
    }

    public String getCurrency() {
        return currency;
    }

    public String getName() {
        return name;
    }

    /** A/D
     *
     */

    @DatabaseField(generatedId = true)
    private long id;
    @DatabaseField(columnName = "name", canBeNull = false)
    private String name;
    @DatabaseField(columnName = "date", canBeNull = false)
    private String date;
    @DatabaseField(columnName = "pictureid", canBeNull = false)
    private int picture_ID;
    @DatabaseField(columnName = "currency", canBeNull = false)
    private String currency;
}
