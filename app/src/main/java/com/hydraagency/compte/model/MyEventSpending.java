package com.hydraagency.compte.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by yolo on 12/8/14.
 */
@DatabaseTable(tableName = "myeventspending")
public class MyEventSpending {

    /** Getter/Setter
     *
     */

    public long getMyEvent_ID() {
        return myEvent_ID;
    }

    public long getMyEventSpending_ID() {
        return id;
    }

    public long getSpending_ID() {
        return spending_ID;
    }

    /** A/D
     *
     */
    @DatabaseField(generatedId = true)
    private long id;
    @DatabaseField(columnName = "myeventid", canBeNull = false)
    private long myEvent_ID;
    @DatabaseField(columnName = "spendingid", canBeNull = false)
    private long spending_ID;
}
