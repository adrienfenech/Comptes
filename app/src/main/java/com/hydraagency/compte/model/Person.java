package com.hydraagency.compte.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by yolo on 11/27/14.
 */
@DatabaseTable(tableName = "person")
public class Person {
    public Person(String name, int parts, int pictureId) {
        this.Name = name;
        this.parts = parts;
        this.pictureId = pictureId;
    }

    public long getPerson_ID() {
        return id;
    }

    public int getParts() {
        return parts;
    }

    public String getName() {
        return Name;
    }

    public int getPictureId() {
        return pictureId;
    }

    /** A/D
     *
     */

    @DatabaseField(generatedId = true)
    private long id;
    @DatabaseField(columnName = "name", canBeNull = false)
    private String Name;
    @DatabaseField(columnName = "parts", canBeNull = false)
    private int parts;
    @DatabaseField(columnName = "pictureid", canBeNull = false)
    private int pictureId;
}
