package com.hydraagency.compte.model;

import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

/**
 * Created by yolo on 12/8/14.
 */
@DatabaseTable(tableName = "myeventperson")
public class MyEventPerson {

    /** Getter/Setter
     *
     */

    public long getPerson_ID() {
        return person_ID;
    }

    public long getMyEvent_ID() {
        return myEvent_ID;
    }

    public long getMyEventPerson_ID() {
        return id;
    }

    /** A/D
     *
     */
    @DatabaseField(generatedId = true)
    private long id;
    @DatabaseField(columnName = "myeventid", canBeNull = false)
    private long myEvent_ID;
    @DatabaseField(columnName = "personid", canBeNull = false)
    private long person_ID;
}
