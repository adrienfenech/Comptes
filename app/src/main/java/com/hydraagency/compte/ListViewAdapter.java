package com.hydraagency.compte;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.hydraagency.compte.model.MyEvent;

import java.util.ArrayList;

/**
 * Created by yolo on 11/27/14.
 */
public class ListViewAdapter extends ArrayAdapter<MyEvent> {
    public ListViewAdapter(Context context, ArrayList<MyEvent> events) {
        super(context, R.layout.list_event_item, events);
        this.context = context;
        this.events = events;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.list_event_item, parent, false);

        TextView nameTV = (TextView)rowView.findViewById(R.id.name);
        TextView peopleTV = (TextView)rowView.findViewById(R.id.people);
        TextView moneyTV = (TextView)rowView.findViewById(R.id.money);
        ImageView pictureIV = (ImageView)rowView.findViewById(R.id.picture);

        nameTV.setText(events.get(position).getName());
        peopleTV.setText(events.get(position).getPeople().size() + (events.get(position).getPeople().size() > 1 ? " participants" : " participant"));
        moneyTV.setText("Total dépenses : " + events.get(position).getMoney() + " " + events.get(position).getCurrency());
        //pictureIV.setBackgroundResource(events.get(position).getPictureId());

        return rowView;
    }

    Context context;
    ArrayList<MyEvent> events;
}
