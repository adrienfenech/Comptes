package com.hydraagency.compte.database;

import android.content.Context;

import com.hydraagency.compte.model.MyEvent;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by yolo on 12/8/14.
 */
public class DBManager {
    private static DBManager ourInstance;

    public static void Init(Context context) {
        if (ourInstance == null)
            ourInstance = new DBManager(context);
    }

    public static DBManager getInstance() {
        return ourInstance;
    }

    private DBManager(Context context) {
        helper = new DatabaseHelper(context);
    }

    private DatabaseHelper getHelper() {
        return helper;
    }

    /** Methods [MyEvent]
     *
     */

    public List<MyEvent> getAllEvents() {
        try {
            return getHelper().getMyEventDao().queryForAll();
        } catch (SQLException e) {
            e.printStackTrace();
            return new ArrayList<MyEvent>();
        }
    }

    public void createEvent(MyEvent myEvent) {
        try {
            getHelper().getMyEventDao().create(myEvent);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void removeEvents(Collection<MyEvent> myEvents) {
        try {
            getHelper().getMyEventDao().delete(myEvents);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public void updateEvent(MyEvent myEvent) {
        try {
            getHelper().getMyEventDao().update(myEvent);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }


    /** A/D
     *
     */
    private DatabaseHelper  helper;
}
