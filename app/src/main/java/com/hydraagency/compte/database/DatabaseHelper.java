package com.hydraagency.compte.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;


import com.hydraagency.compte.model.MyEvent;
import com.hydraagency.compte.model.MyEventPerson;
import com.hydraagency.compte.model.MyEventPersonSpendingBy;
import com.hydraagency.compte.model.MyEventPersonSpendingFor;
import com.hydraagency.compte.model.MyEventSpending;
import com.hydraagency.compte.model.Person;
import com.hydraagency.compte.model.Spending;
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper;
import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.table.TableUtils;

import java.sql.SQLException;

/**
 * Created by yolo on 12/8/14.
 */
public class DatabaseHelper extends OrmLiteSqliteOpenHelper {
    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase, com.j256.ormlite.support.ConnectionSource connectionSource) {
        try {
            TableUtils.createTable(connectionSource, Person.class);
            TableUtils.createTable(connectionSource, Spending.class);
            TableUtils.createTable(connectionSource, MyEvent.class);
            TableUtils.createTable(connectionSource, MyEventPerson.class);
            TableUtils.createTable(connectionSource, MyEventPersonSpendingBy.class);
            TableUtils.createTable(connectionSource, MyEventPersonSpendingFor.class);
            TableUtils.createTable(connectionSource, MyEventSpending.class);
        } catch (java.sql.SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, com.j256.ormlite.support.ConnectionSource connectionSource, int i, int i2) {
        try {
            TableUtils.dropTable(connectionSource, Person.class, true);
            TableUtils.dropTable(connectionSource, Spending.class, true);
            TableUtils.dropTable(connectionSource, MyEvent.class, true);
            TableUtils.dropTable(connectionSource, MyEventPerson.class, true);
            TableUtils.dropTable(connectionSource, MyEventPersonSpendingBy.class, true);
            TableUtils.dropTable(connectionSource, MyEventPersonSpendingFor.class, true);
            TableUtils.dropTable(connectionSource, MyEventSpending.class, true);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    /** Getter/Setter
     *
     */

    public Dao<MyEvent, Integer> getMyEventDao() {
        if (myEventDAO == null) {
            try {
                myEventDAO = getDao(MyEvent.class);
            }catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return myEventDAO;
    }
    public Dao<MyEventPerson, Integer> getMyEventPersonDao() {
        if (myEventPersonDAO == null) {
            try {
                myEventPersonDAO = getDao(MyEventPerson.class);
            }catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return myEventPersonDAO;
    }
    public Dao<MyEventPersonSpendingBy, Integer> getMyEventPersonSpendingByDao() {
        if (myEventPersonSpendingByDAO == null) {
            try {
                myEventPersonSpendingByDAO = getDao(MyEventPersonSpendingBy.class);
            }catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return myEventPersonSpendingByDAO;
    }
    public Dao<MyEventPersonSpendingFor, Integer> getmyEventPersonSpendingForDao() {
        if (myEventPersonSpendingForDAO == null) {
            try {
                myEventPersonSpendingForDAO = getDao(MyEventPersonSpendingFor.class);
            }catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return myEventPersonSpendingForDAO;
    }
    public Dao<MyEventSpending, Integer> getMyEventSpendingDao() {
        if (myEventSpendingDAO == null) {
            try {
                myEventSpendingDAO = getDao(MyEventSpending.class);
            }catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return myEventSpendingDAO;
    }
    public Dao<Person, Integer> getPersonDao() {
        if (personDAO == null) {
            try {
               personDAO = getDao(Person.class);
            }catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return personDAO;
    }
    public Dao<Spending, Integer> getSpendingDao() {
        if (spendingDAO == null) {
            try {
               spendingDAO = getDao(Spending.class);
            }catch (java.sql.SQLException e) {
                e.printStackTrace();
            }
        }
        return spendingDAO;
    }

    /** A/D
     *
     */

    private Dao<MyEvent, Integer>                   myEventDAO = null;
    private Dao<MyEventPerson, Integer>             myEventPersonDAO= null;
    private Dao<MyEventPersonSpendingBy, Integer>   myEventPersonSpendingByDAO= null;
    private Dao<MyEventPersonSpendingFor, Integer>  myEventPersonSpendingForDAO= null;
    private Dao<MyEventSpending, Integer>           myEventSpendingDAO= null;
    private Dao<Person, Integer>                    personDAO= null;
    private Dao<Spending, Integer>                  spendingDAO= null;


    /** CONSTANT
     *
     */
    private static final String     DB_NAME = "bonCompte.sqlite";
    private static final int        DB_VERSION = 3;
}
