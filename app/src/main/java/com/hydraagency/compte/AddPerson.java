package com.hydraagency.compte;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.hydraagency.compte.model.Person;

/**
 * Created by yolo on 11/26/14.
 */
public class AddPerson extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_person);
        final EditText et = (EditText)findViewById(R.id.name);
        final ImageButton ib = (ImageButton)findViewById(R.id.picture);
        final NumberPicker nb = (NumberPicker)findViewById(R.id.numberpicker);
        nb.setMinValue(1);
        nb.setMaxValue(10);

        mine = this;
        Button save = (Button)findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (et.getText().toString().length() != 0) {
                    Person person = new Person(et.getText().toString(), nb.getValue(), 0);
                    Information.getInstance().addPerson(person);
                    finish();
                }
                else
                    Toast.makeText(mine, "Fill the name's input.", Toast.LENGTH_LONG).show();
            }
        });
    }

    /** A/D
     *
     */

    Activity mine;
}
