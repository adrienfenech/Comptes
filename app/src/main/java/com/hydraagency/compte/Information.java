package com.hydraagency.compte;

import com.hydraagency.compte.model.MyEvent;
import com.hydraagency.compte.model.Person;

import java.util.ArrayList;

/**
 * Created by yolo on 12/1/14.
 */
public class Information {
    private static Information ourInstance = new Information();

    public static Information getInstance() {
        return ourInstance;
    }

    private Information() {
    }

    public ArrayList<MyEvent> getEvents() {
        return events;
    }

    public ArrayList<Person> getPeople() {
        return people;
    }

    public void setEvents(ArrayList<MyEvent> events) {
        this.events = events;
    }

    public void setPeople(ArrayList<Person> people) {
        this.people = people;
    }

    public void initialize() {
        people = new ArrayList<Person>() {{
            add(new Person("Jean", 1, 0));
            add(new Person("Toto", 2, 0));
            add(new Person("Bernard", 1, 0));
            add(new Person("Yolo", 2, 0));
            add(new Person("Suzette", 4, 0));
        }};
        events = new ArrayList<MyEvent>() {{
            add(new MyEvent("Test 1",
                    new ArrayList<Person>() {{
                        add(people.get(0));
                        add(people.get(1));
                    }},
                    (float)12.5,
                    "EUR",
                    0));
            add(new MyEvent("Test 2",
                    new ArrayList<Person>() {{
                        add(people.get(2));
                        add(people.get(3));
                        add(people.get(4));
                    }},
                    (float)18.59,
                    "EUR",
                    0));
        }};
    }

    public void addPerson(Person person) { this.people.add(person); }
    public void addEvent(MyEvent event) { this.events.add(event); }

    /** A/D
     *
     */
    private ArrayList<Person>   people;
    private ArrayList<MyEvent>  events;
}
