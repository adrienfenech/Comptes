package com.hydraagency.compte;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.NumberPicker;
import android.widget.Toast;

import com.hydraagency.compte.model.MyEvent;
import com.hydraagency.compte.model.Person;

import java.lang.reflect.Field;
import java.util.ArrayList;

/**
 * Created by yolo on 11/26/14.
 */
public class AddEvent extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_event);
        final NumberPicker nb = (NumberPicker)findViewById(R.id.numberpicker);
        final String[] keys = new String[] {
                "EUR",
                "GBP",
                "USD",
                "JPY",
                "ARS",
                "AUD",
                "BSD",
                "BHD",
                "BBD",
                "XOF",
                "BRL",
                "XAF",
                "CAD",
                "CLP",
                "CNY",
                "HRK",
                "CZK",
                "DKK",
                "XCD",
                "EGP",
                "EEK",
                "FJD",
                "HKD",
                "HUF",
                "ISK",
                "INR",
                "IDR",
                "ILS",
                "JMD",
                "KES",
                "KRW",
                "LVL",
                "MYR",
                "MXN",
                "MAD",
                "ANG",
                "NZD",
                "NOK",
                "OMR",
                "PKR",
                "PAB",
                "PEN",
                "PHP",
                "PLN",
                "QAR",
                "RON",
                "RUB",
                "SAR",
                "RSD",
                "SGD",
                "ZAR",
                "LKR",
                "SEK",
                "CHF",
                "TWD",
                "THB",
                "TTD",
                "TRY",
                "AED",
                "VEF",
                "VND"
        };
        String[] tabs = new String[keys.length];
        for (int i = 0; i < keys.length; i++)
            tabs[i] = keys[i] + " - " + getResources().getString(getId(keys[i], R.string.class));

        nb.setMinValue(1);
        nb.setMaxValue(tabs.length);
        nb.setDisplayedValues(tabs);

        final EditText tv = (EditText)findViewById(R.id.name);

        mine = this;
        Button save = (Button)findViewById(R.id.save);
        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (tv.getText().toString().length() != 0) {
                    MyEvent event = new MyEvent(tv.getText().toString(), new ArrayList<Person>(), 0f, keys[nb.getValue() - 1], R.drawable.ic_launcher);
                    Information.getInstance().addEvent(event);
                    Intent returnIntent = new Intent();
                    setResult(RESULT_OK, returnIntent);
                    finish();
                }
                else
                    Toast.makeText(mine, "Fill the name's input.", Toast.LENGTH_LONG).show();
            }
        });
    }

    private int getId(String name, Class<?> c) {
        int id = -1;
        try {
            Field field = c.getField(name);
            id = field.getInt(null);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }

    private Activity mine;
}
