package com.hydraagency.compte;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.GridView;


public class MainActivity extends ActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Information.getInstance().initialize();

        mine = this;

        View.OnClickListener addEvent = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mine, AddEvent.class);
                startActivity(intent);
            }
        };
        View.OnClickListener addPerson = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mine, AddPerson.class);
                startActivity(intent);
            }
        };
        View.OnClickListener listEvent = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mine, ListEvent.class);
                startActivity(intent);
            }
        };

        findViewById(R.id.addeventimage).setOnClickListener(addEvent);
        findViewById(R.id.addeventtext).setOnClickListener(addEvent);
        findViewById(R.id.listeventimage).setOnClickListener(listEvent);
        findViewById(R.id.listeventtext).setOnClickListener(listEvent);
        findViewById(R.id.addpersonimage).setOnClickListener(addPerson);
        findViewById(R.id.addpersontext).setOnClickListener(addPerson);

        final GridView gv = (GridView) findViewById(R.id.content);
        mine = this;
        gv.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                gv.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                gv.setAdapter(new GridAdapter(mine, gv.getWidth(), gv.getHeight()));
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    Activity mine;
}
