package com.hydraagency.compte;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created by yolo on 11/26/14.
 */
public class ListEvent extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list_event);

        ListView lv = (ListView)findViewById(R.id.content);
        lv.setAdapter(new ListViewAdapter(this, Information.getInstance().getEvents()));

        mine = this;
        View.OnClickListener addEvent = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mine, AddEvent.class);
                startActivityForResult(intent, 1);
            }
        };
        findViewById(R.id.addeventimage).setOnClickListener(addEvent);
        findViewById(R.id.addeventtext).setOnClickListener(addEvent);
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {

            if(resultCode == RESULT_OK) {
                ListView lv = (ListView)findViewById(R.id.content);
                lv.setAdapter(new ListViewAdapter(this, Information.getInstance().getEvents()));
            }
        }
    }

    private Activity mine;
}
